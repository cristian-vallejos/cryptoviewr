# CryptoViewr #

CryptoViewr es una aplicación móvil compatible con dispositivos Android 5.1 hacia adelante. Fue desarrollada en el lenguaje de programación Kotlin bajo el patrón de arquitectura de software MVVM (Model-View-ViewModel).

### Decisiones tomadas ###

* En primer lugar, se decidió incorporar una pantalla de inicio para dar la bienvenida al usuario.

* Se decidió suprimir la AppBar, ya que no prestaba mayor utilidad al diseño de la app.

* En primera instancia, el listado del Top 10 de criptomonedas fue implementado con una RecyclerView básica, cuyo diseño era prácticamente el de una lista común y corriente. Sin embargo, en un segundo alcance, se implementó de tal forma que el usuario pudiese deslizar hacia un lado y visualizar el siguiente elemento de la lista.

* Se decidió incorporar un botón que permitiese al usuario visitar el sitio de CoinMarketCap, en caso de que necesite saber más acerca de una criptomoneda en específico (para ello sólo bastó con estudiar la uri de la web y splittear los nombres de las cryptos).

* También se añadió un Spinner, el cual permite seleccionar si se desea visualizar el valor de las criptomonedas en CLP o USD.

* Por último, se incorporó un dato en la parte inferior, el cual está relacionado con la última actualización. Esto debido a que el usuario podría querer visualizar el valor de las cryptos en CLP o USD y podría querer estar en conocimiento de que los datos mostrados se obtengan en tiempo real.

### Funcionalidades potenciales ###

* Se podría agregar una nueva vista que permita visualizar un historial más detallado al seleccionar una criptomoneda específica.

* También se podría agregar otra vista que permita buscar criptomonedas, ya que no todas las personas están familiarizadas con aquellas que forman parte del Top10.

* Se podría añadir la opción de creación de perfiles, con el objetivo de que los usuarios pudiesen seleccionar su cryptos de interés y así la app pudiese informarles la variación de éstas de una manera más personalizada.

* Podría añadirse una funcionalidad para estar al tanto de cuáles son las cryptos que más se mueven en el mercado, independiente de si su valor es alto o bajo.

### Posibles bugs ###

* Al abrir la aplicación sin estar conectada a la red, es posible que se quede esperando y cargando, incluso aún cuando el usuario restablezca su conexión.
* Al cambiar la moneda seleccionada, el RecyclerView se resetea y se deja de visualizar la criptomoneda que en ese momento se estaba visualizando. En su lugar, se vuelve a la #1.

Ante cualquier duda o consulta respecto de esta app, no dude en ponerse en contacto vía mail a la siguiente dirección: cristian.vallejos@live.com