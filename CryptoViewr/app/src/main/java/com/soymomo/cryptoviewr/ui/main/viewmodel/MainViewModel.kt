package com.soymomo.cryptoviewr.ui.main.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.soymomo.cryptoviewr.data.model.Crypto
import com.soymomo.cryptoviewr.data.repository.MainRepository
import com.soymomo.cryptoviewr.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(private val mainRepository: MainRepository) : ViewModel() {
    var currency = "CLP"
    set(curr){
        if (field != curr){
            field  = curr
            fetchCryptos(curr)
        }
    }

    private val cryptos = MutableLiveData<Resource<List<Crypto>>>()
    private val compositeDisposable = CompositeDisposable()

    init {
        fetchCryptos(currency)
    }

    private fun fetchCryptos(curren: String) {
        cryptos.postValue(Resource.loading(null))
        compositeDisposable.add(
            mainRepository.getResponse(curren)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    var count: Int = 1
                    for (i in response.data.indices) {
                       response.data[i].cmc_rank = count
                        count++
                    }
                    cryptos.postValue(Resource.success(response.data))
                }, { throwable ->
                    cryptos.postValue(Resource.error("Por favor, revisa tu conexión a Internet y vuelve a intentarlo", null))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getCryptos(): LiveData<Resource<List<Crypto>>> {
        return cryptos
    }
}