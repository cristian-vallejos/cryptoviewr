package com.soymomo.cryptoviewr.data.model
import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("status")
    var status: Status = Status(),
    @SerializedName("data")
    var data: List<Crypto> = ArrayList()
)

data class Status(
    @SerializedName("error_message")
    var error_message: String = ""
)

data class Crypto(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("cmc_rank")
    var cmc_rank: Int = 0,
    @SerializedName("quote")
    var quote: Quote = Quote()
)

data class Quote(
    @SerializedName("CLP")
    var chilean: CLP = CLP(),
    @SerializedName("USD")
    var dolar: USD = USD()
)

data class CLP(
    @SerializedName("price")
    var price: Double = 0.0,
    @SerializedName("percent_change_1h")
    var percent_change_1h: Double = 0.0,
    @SerializedName("percent_change_24h")
    var percent_change_24h: Double = 0.0,
    @SerializedName("percent_change_7d")
    var percent_change_7d: Double = 0.0,
    @SerializedName("percent_change_30d")
    var percent_change_30d: Double = 0.0
)

data class USD(
    @SerializedName("price")
    var price: Double = 0.0,
    @SerializedName("percent_change_1h")
    var percent_change_1h: Double = 0.0,
    @SerializedName("percent_change_24h")
    var percent_change_24h: Double = 0.0,
    @SerializedName("percent_change_7d")
    var percent_change_7d: Double = 0.0,
    @SerializedName("percent_change_30d")
    var percent_change_30d: Double = 0.0
)