package com.soymomo.cryptoviewr.data.api

import com.soymomo.cryptoviewr.data.model.Response
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Single

class ApiServiceImpl : ApiService {
    override fun getResponse(currencyParam: String): Single<Response> {
        return Rx2AndroidNetworking.get("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=10&sort=price&sort_dir=desc&convert=$currencyParam")
            .addHeaders("Accepts", "application/json")
            .addHeaders("X-CMC_PRO_API_KEY", "ca18ab5c-45ad-459d-9ed3-903b6164164d")
            .build()
            .getObjectSingle(Response::class.java)
    }
}