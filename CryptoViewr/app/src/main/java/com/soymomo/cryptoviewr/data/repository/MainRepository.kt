package com.soymomo.cryptoviewr.data.repository

import com.soymomo.cryptoviewr.data.api.ApiHelper
import com.soymomo.cryptoviewr.data.model.Response
import io.reactivex.Single

class MainRepository(private val apiHelper: ApiHelper) {
    fun getResponse(currencyParam: String): Single<Response> {
        return apiHelper.getResponse(currencyParam)
    }
}