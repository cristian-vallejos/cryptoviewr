package com.soymomo.cryptoviewr.data.api

class ApiHelper(private val apiService: ApiService) {
    fun getResponse(currencyParam: String) = apiService.getResponse(currencyParam)
}