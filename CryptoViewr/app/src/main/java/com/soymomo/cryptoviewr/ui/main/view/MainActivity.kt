package com.soymomo.cryptoviewr.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.SnapHelper
import com.soymomo.cryptoviewr.R
import com.soymomo.cryptoviewr.data.api.ApiHelper
import com.soymomo.cryptoviewr.data.api.ApiServiceImpl
import com.soymomo.cryptoviewr.data.model.Crypto
import com.soymomo.cryptoviewr.ui.base.ViewModelFactory
import com.soymomo.cryptoviewr.ui.main.adapter.MainAdapter
import com.soymomo.cryptoviewr.ui.main.viewmodel.MainViewModel
import com.soymomo.cryptoviewr.utils.Status
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var adapter: MainAdapter
    var HorizontalLayout: LinearLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            this.supportActionBar!!.hide()
        } catch (e: NullPointerException) {}

        setContentView(R.layout.activity_main)
        setupUI()
    }

    override fun onStart() {
        super.onStart()
        setupViewModel()
        setupObserver()
    }

    override fun onResume() {
        super.onResume()
        setupViewModel()
        setupObserver()
    }

    private fun setupUI() {
        /** Se configura el spinner para que, al cambiar de moneda, se actualicen los datos **/
        val currencies = resources.getStringArray(R.array.Currencies)
        if (spinnerCurrency != null) {
            val adapter = ArrayAdapter(this,
                android.R.layout.simple_spinner_dropdown_item, currencies)
            spinnerCurrency.adapter = adapter
        }

        spinnerCurrency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                mainViewModel.currency = spinnerCurrency.selectedItem.toString()
                adapter = MainAdapter(arrayListOf(), spinnerCurrency.selectedItem.toString())
                recyclerCryptos.addItemDecoration(
                    DividerItemDecoration(
                        recyclerCryptos.context,
                        (recyclerCryptos.layoutManager as LinearLayoutManager).orientation
                    )
                )
                recyclerCryptos.adapter = adapter

                setupViewModel()
                setupObserver()
            }
        }

        /** Se configura el recycler para que actualice los valores correspondientes a cada criptomoneda **/
        recyclerCryptos.layoutManager = LinearLayoutManager(this)
        adapter = MainAdapter(arrayListOf(), spinnerCurrency.selectedItem.toString())
        recyclerCryptos.addItemDecoration(
            DividerItemDecoration(
                recyclerCryptos.context,
                (recyclerCryptos.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerCryptos.adapter = adapter

        /** Se configura el spinner para remover la línea divisora entre items **/
        val horizontalDecoration = DividerItemDecoration(
            recyclerCryptos.getContext(),
            DividerItemDecoration.VERTICAL
        )

        val horizontalDivider =
            ContextCompat.getDrawable(this@MainActivity, R.drawable.crypto_divider)
        horizontalDecoration.setDrawable(horizontalDivider!!)
        recyclerCryptos.addItemDecoration(horizontalDecoration)

        /** Se configura el spinner para que el scroll sea horizontal **/
        HorizontalLayout = LinearLayoutManager(
            this@MainActivity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerCryptos.setLayoutManager(HorizontalLayout)

        /** Se configura el spinner para que tienda a centrar las tarjetas **/
        val helper: SnapHelper = LinearSnapHelper()
        helper.attachToRecyclerView(recyclerCryptos)
    }

    private fun setupObserver() {
        mainViewModel.getCryptos().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { cryptos -> renderList(cryptos) }
                    loadingCryptos.visibility = View.GONE
                    recyclerCryptos.visibility = View.VISIBLE

                    val rightNow: Calendar = Calendar.getInstance()
                    val currentHourIn24Format: Int = rightNow.get(Calendar.HOUR_OF_DAY)
                    val currentMin: Int = rightNow.get(Calendar.MINUTE)

                    updateHour.text = "Última actualización a las $currentHourIn24Format:$currentMin"
                }

                Status.LOADING -> {
                    loadingCryptos.visibility = View.VISIBLE
                    recyclerCryptos.visibility = View.GONE
                }

                Status.ERROR -> {
                    //Handle Error
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun renderList(cryptos: List<Crypto>) {
        adapter.clearData()
        adapter.addData(cryptos)
        adapter.notifyDataSetChanged()
    }

    private fun setupViewModel() {
        mainViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(ApiServiceImpl()))
        ).get(MainViewModel::class.java)
    }
}