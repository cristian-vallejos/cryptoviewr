package com.soymomo.cryptoviewr.ui.main.adapter
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.soymomo.cryptoviewr.R
import com.soymomo.cryptoviewr.data.model.Crypto
import kotlinx.android.synthetic.main.crypto_item_layout.view.*
import java.security.AccessController.getContext
import java.text.DecimalFormat

class MainAdapter(private val cryptos: ArrayList<Crypto>, currency: String) : RecyclerView.Adapter<MainAdapter.DataViewHolder>() {
    var curr = currency
    set(curr){
        if (field != curr){
            field  = curr
        }
    }

    class DataViewHolder(itemView: View, curr: String) : RecyclerView.ViewHolder(itemView) {
        var sigla = curr
        fun roundTwoDecimals(d: Double): String {
            val twoDForm : DecimalFormat
            if (sigla == "CLP"){
                twoDForm = DecimalFormat("#,###,###")
            }
            else {
                twoDForm = DecimalFormat("#,###,###.##")
            }
            return twoDForm.format(d)
        }

        fun roundPercentage(d: Double): String {
            return DecimalFormat("#,###,###").format(d)
        }


        @SuppressLint("SetTextI18n")
        fun bind(crypto: Crypto) {
            itemView.cryptoRank.text = "#"+crypto.cmc_rank.toString()

            itemView.leftArrow.visibility = View.VISIBLE
            itemView.rightArrow.visibility = View.VISIBLE
            if (crypto.cmc_rank == 1) {
                itemView.leftArrow.visibility = View.GONE
                itemView.cryptoRank.setTextColor(Color.parseColor("#FFD700"))
            } else if (crypto.cmc_rank == 2) {
                itemView.cryptoRank.setTextColor(Color.parseColor("#C0C0C0"))
            } else if (crypto.cmc_rank == 3) {
                itemView.cryptoRank.setTextColor(Color.parseColor("#B08D57"))
            } else {
                if (crypto.cmc_rank == 10)
                    itemView.rightArrow.visibility = View.GONE
                itemView.cryptoRank.setTextColor(Color.parseColor("#B19CD9"))
            }

            itemView.cryptoName.text = crypto.name

            if (sigla == "CLP"){
                itemView.cryptoPrice.text = "$"+roundTwoDecimals(crypto.quote.chilean.price)

                when {
                    crypto.quote.chilean.percent_change_1h > 0.0 -> {
                        val change = roundPercentage(crypto.quote.chilean.percent_change_1h)
                        itemView.cryptoChange1h.text = "+$change%"
                        itemView.cryptoChange1h.setTextColor(Color.parseColor("#4CAF50"))
                    }
                    crypto.quote.chilean.percent_change_1h < 0.0 -> {
                        val change = roundPercentage(crypto.quote.chilean.percent_change_1h)
                        itemView.cryptoChange1h.text = "$change%"
                        itemView.cryptoChange1h.setTextColor(Color.parseColor("#E91E63"))
                    }
                }

                when {
                    crypto.quote.chilean.percent_change_24h > 0.0 -> {
                        val change = roundPercentage(crypto.quote.chilean.percent_change_24h)
                        itemView.cryptoChange24h.text = "+$change%"
                        itemView.cryptoChange24h.setTextColor(Color.parseColor("#4CAF50"))
                    }
                    crypto.quote.chilean.percent_change_24h < 0.0 -> {
                        val change = roundPercentage(crypto.quote.chilean.percent_change_24h)
                        itemView.cryptoChange24h.text = "$change%"
                        itemView.cryptoChange24h.setTextColor(Color.parseColor("#E91E63"))
                    }
                }

                when {
                    crypto.quote.chilean.percent_change_7d > 0.0 -> {
                        val change = roundPercentage(crypto.quote.chilean.percent_change_7d)
                        itemView.cryptoChange7d.text = "+$change%"
                        itemView.cryptoChange7d.setTextColor(Color.parseColor("#4CAF50"))
                    }
                    crypto.quote.chilean.percent_change_7d < 0.0 -> {
                        val change = roundPercentage(crypto.quote.chilean.percent_change_7d)
                        itemView.cryptoChange7d.text = "$change%"
                        itemView.cryptoChange7d.setTextColor(Color.parseColor("#E91E63"))
                    }
                }

                when {
                    crypto.quote.chilean.percent_change_30d > 0.0 -> {
                        val change = roundPercentage(crypto.quote.chilean.percent_change_30d)
                        itemView.cryptoChange30d.text = "+$change%"
                        itemView.cryptoChange30d.setTextColor(Color.parseColor("#4CAF50"))
                    }
                    crypto.quote.chilean.percent_change_7d < 0.0 -> {
                        val change = roundPercentage(crypto.quote.chilean.percent_change_30d)
                        itemView.cryptoChange30d.text = "$change%"
                        itemView.cryptoChange30d.setTextColor(Color.parseColor("#E91E63"))
                    }
                }
            }
            else {
                itemView.cryptoPrice.text = "$"+roundTwoDecimals(crypto.quote.dolar.price)

                when {
                    crypto.quote.dolar.percent_change_1h > 0.0 -> {
                        val change = roundPercentage(crypto.quote.dolar.percent_change_1h)
                        itemView.cryptoChange1h.text = "+$change%"
                        itemView.cryptoChange1h.setTextColor(Color.parseColor("#4CAF50"))
                    }
                    crypto.quote.dolar.percent_change_1h < 0.0 -> {
                        val change = roundPercentage(crypto.quote.dolar.percent_change_1h)
                        itemView.cryptoChange1h.text = "$change%"
                        itemView.cryptoChange1h.setTextColor(Color.parseColor("#E91E63"))
                    }
                }

                when {
                    crypto.quote.dolar.percent_change_24h > 0.0 -> {
                        val change = roundPercentage(crypto.quote.dolar.percent_change_24h)
                        itemView.cryptoChange24h.text = "+$change%"
                        itemView.cryptoChange24h.setTextColor(Color.parseColor("#4CAF50"))
                    }
                    crypto.quote.dolar.percent_change_24h < 0.0 -> {
                        val change = roundPercentage(crypto.quote.dolar.percent_change_24h)
                        itemView.cryptoChange24h.text = "$change%"
                        itemView.cryptoChange24h.setTextColor(Color.parseColor("#E91E63"))
                    }
                }

                when {
                    crypto.quote.dolar.percent_change_7d > 0.0 -> {
                        val change = roundPercentage(crypto.quote.dolar.percent_change_7d)
                        itemView.cryptoChange7d.text = "+$change%"
                        itemView.cryptoChange7d.setTextColor(Color.parseColor("#4CAF50"))
                    }
                    crypto.quote.dolar.percent_change_7d < 0.0 -> {
                        val change = roundPercentage(crypto.quote.dolar.percent_change_7d)
                        itemView.cryptoChange7d.text = "$change%"
                        itemView.cryptoChange7d.setTextColor(Color.parseColor("#E91E63"))
                    }
                }

                when {
                    crypto.quote.dolar.percent_change_30d > 0.0 -> {
                        val change = roundPercentage(crypto.quote.dolar.percent_change_30d)
                        itemView.cryptoChange30d.text = "+$change%"
                        itemView.cryptoChange30d.setTextColor(Color.parseColor("#4CAF50"))
                    }
                    crypto.quote.dolar.percent_change_7d < 0.0 -> {
                        val change = roundPercentage(crypto.quote.dolar.percent_change_30d)
                        itemView.cryptoChange30d.text = "$change%"
                        itemView.cryptoChange30d.setTextColor(Color.parseColor("#E91E63"))
                    }
                }
            }

            itemView.goToSite.setOnClickListener(){
                val url = "https://coinmarketcap.com/es/currencies/"
                var uri = ""
                val name_splitted = crypto.name.split(" ")
                for (word in name_splitted) {
                    uri += if (word != name_splitted[name_splitted.size-1])
                        "$word-"
                    else
                        word
                }

                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url+uri)
                itemView.goToSite.context.startActivity(i)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.crypto_item_layout, parent,
                false
            ), curr
        )

    override fun getItemCount(): Int = cryptos.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) = holder.bind(cryptos[position])

    fun addData(list: List<Crypto>) {
        cryptos.addAll(list)
    }

    fun clearData(){
        cryptos.clear()
    }
}