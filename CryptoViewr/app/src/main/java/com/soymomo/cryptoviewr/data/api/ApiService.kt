package com.soymomo.cryptoviewr.data.api
import com.soymomo.cryptoviewr.data.model.Response
import io.reactivex.Single

interface ApiService {
    fun getResponse(currencyParam: String): Single<Response>
}