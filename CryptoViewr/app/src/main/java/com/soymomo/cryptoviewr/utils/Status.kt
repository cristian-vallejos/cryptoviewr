package com.soymomo.cryptoviewr.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}